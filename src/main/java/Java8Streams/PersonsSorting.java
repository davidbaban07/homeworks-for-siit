package Java8Streams;

import java.io.*;
import java.util.*;

public class PersonsSorting {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        PersonsSorting personsSorting =new PersonsSorting();
        personsSorting.readPersons("src/main/resources/in.txt");
        personsSorting.orderdByMonthStream(5);
    }
    List<Persons> personsList;
    Set<Persons> orderedPersons;

    public PersonsSorting() {
        personsList = new ArrayList<>();
    }

    public void readPersons(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;
        while ((line = reader.readLine()) != null) {
            Persons persons = retrivePersons(line);
            personsList.add(persons);
        }
    }

    public void orderdByMonthStream(int month) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File("src/main/resources/out.txt")));
        List<Persons> personsListed = personsList
                .stream()
                .filter(persons -> {
                    return persons.getMonth() == month;
                })
                .sorted(Comparator.comparing(Persons::getFirstName)).toList();

        bufferedWriter.write(String.valueOf(personsListed));
        bufferedWriter.close();
    }

    private Persons retrivePersons(String line) {
        Persons persons = new Persons();
        Scanner s = new Scanner(line);
        s.useDelimiter(" ");
        if (s.hasNext()) {
            persons.setFirstName(s.next());
            persons.setLastName(s.next());
            persons.setDateOfBirth(s.next());
            persons.setMonthValue();
        }
        return persons;
    }

}