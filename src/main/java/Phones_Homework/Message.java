package Phones_Homework;

public class Message {

    String phoneNumber, msgContent;

    public Message(String phoneNumber, String msgContent) {
        this.phoneNumber = phoneNumber;
        this.msgContent = msgContent;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    @Override
    public String toString() {
        return "Message{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", msgContent='" + msgContent + '\'' +
                '}';
    }
}
