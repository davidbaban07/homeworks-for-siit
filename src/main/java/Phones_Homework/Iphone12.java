package Phones_Homework;

public class Iphone12 extends Apple {

    @Override
    public String toString() {
        return "Iphone12{" +
                "manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", batteryLife=" + batteryLife +
                ", color='" + color + '\'' +
                ", material='" + material + '\'' +
                ", IMEI=" + IMEI +
                '}';
    }
}
