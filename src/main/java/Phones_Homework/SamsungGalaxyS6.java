package Phones_Homework;

public class SamsungGalaxyS6 extends Samsung{

    @Override
    public String toString() {
        return "SamsungGalaxyS6{" +
                "manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", batteryLife=" + batteryLife +
                ", color='" + color + '\'' +
                ", material='" + material + '\'' +
                ", IMEI=" + IMEI +
                '}';
    }
}
