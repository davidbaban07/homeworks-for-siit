package Phones_Homework;

public class Call {
    String phoneNumber;

    @Override
    public String toString() {
        return "Call{" +
                "phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    public Call(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
