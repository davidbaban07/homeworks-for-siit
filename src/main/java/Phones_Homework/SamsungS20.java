package Phones_Homework;

public class SamsungS20 extends Samsung{

    @Override
    public String toString() {
        return "SamsungS20{" +
                "manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", batteryLife=" + batteryLife +
                ", color='" + color + '\'' +
                ", material='" + material + '\'' +
                ", IMEI=" + IMEI +
                '}';
    }
}
