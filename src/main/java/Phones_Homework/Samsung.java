package Phones_Homework;

public abstract class Samsung extends Phone {

    @Override
    public String toString() {
        return "Samsung{" +
                "manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", batteryLife=" + batteryLife +
                ", color='" + color + '\'' +
                ", material='" + material + '\'' +
                ", IMEI=" + IMEI +
                '}';
    }
}
