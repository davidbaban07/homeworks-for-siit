package Phones_Homework;

public interface Behavior {

    void createContact(int index , String phoneNumber , String firstName , String lastName);
    void existingContacts();
    void sendTextMsg(String phoneNumber, String msgContent);
    void msgSpecContact(String phoneNumber, String msgContent);
    void makeCall(String phoneNumber);
    void callHistory();

}
