package Phones_Homework;

import java.util.ArrayList;
import java.util.List;

public class Phone implements Behavior {

    String manufacturer;
    String model;
    int batteryLife;
    String color;
    String material;
    int IMEI;
    int max_lenght = 500;

    List<Contact> listContacts = new ArrayList<>();
    List<Message> listMessages = new ArrayList<>();
    List<Call> listCalls= new ArrayList<>();

    @Override
    public void createContact(int index, String phoneNumber, String firstName, String lastName) {
        listContacts.add(new Contact(index, phoneNumber, firstName, lastName));
    }

    @Override
    public void existingContacts() {
        System.out.println("Lista de contacte este : " + listContacts.toString());
    }

    @Override
    public void sendTextMsg(String phoneNumber, String msgContent) {
        if (msgContent.length() < max_lenght) {
            System.out.println("Mesajul este : " + msgContent);
            listMessages.add(new Message(phoneNumber, msgContent));
        } else{
            System.out.println("Mesajul este prea lung");
        }

    }

    @Override
    public void msgSpecContact(String phoneNumber, String msgContent) {


    }

    @Override
    public void makeCall(String phoneNumber) {
        System.out.println("S-a apelat nuumarul : " + phoneNumber);
        listCalls.add(new Call(phoneNumber));

    }

    @Override
    public void callHistory() {

    }


    @Override
    public String toString() {
        return "Phone{" +
                "manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", batteryLife=" + batteryLife +
                ", color='" + color + '\'' +
                ", material='" + material + '\'' +
                ", IMEI=" + IMEI +
                '}';
    }
}
