package Phones_Homework;

public class IphoneX extends Apple {
    @Override
    public String toString() {
        return "IphoneX{" +
                "manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", batteryLife=" + batteryLife +
                ", color='" + color + '\'' +
                ", material='" + material + '\'' +
                ", IMEI=" + IMEI +
                '}';
    }
}

