package Phones_Homework;

public class Main {
    public static void main(String[] args) {

        Phone phone1 = new Iphone12();
        Phone phone2 = new IphoneX();
        Phone phone3 = new SamsungGalaxyS6();
        Phone phone4 = new SamsungS20();

        phone1.manufacturer = "Apple";
        phone1.model = "Iphone12";
        phone1.batteryLife = 20;
        phone1.color = "black";
        phone1.material = "metal";
        phone1.IMEI = 168473557;

        phone2.manufacturer = "Apple";
        phone2.model = "IphoneX";
        phone2.batteryLife = 17;
        phone2.color = "white";
        phone2.material = "glass";
        phone2.IMEI = 168674354;

        phone3.manufacturer = "Samsung";
        phone3.model = "Samsung Galaxy S6";
        phone3.batteryLife = 15;
        phone3.color = "black";
        phone3.material = "glass";
        phone3.IMEI = 34234566;

        phone4.manufacturer = "Samsung";
        phone4.model = "Samsung S20";
        phone4.batteryLife = 25;
        phone4.color = "Gold";
        phone4.material = "metal";
        phone4.IMEI = 325346456;

        System.out.println("\nTelefonul 1 : " + phone1 + "\nTelefonul 2 :" + phone2 + "\nTelefonul 3: " + phone3 + "\nTelefonul 4 : " + phone4);


        phone1.createContact(1, "0753465642", "David", "Baban");
        phone1.createContact(2, "0755746572", "Alex", "Moldovan");
        phone1.createContact(3, "0755666754", "Silviu", "Petrean");
        phone1.createContact(4, "0755664644", "Andreas", "Toadere");
        phone1.existingContacts();

        phone1.sendTextMsg("0753664668", "Salut ! ");
        phone1.sendTextMsg("0753664368", "t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).");
        phone1.sendTextMsg("0753664668", "Bonjour ! ");

        phone1.makeCall("0744578024");


    }
}
