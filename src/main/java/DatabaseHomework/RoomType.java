public enum RoomType {
    SINGLE,
    DOUBLE,
    SUITE,
    APARTMENT,
    PENTHOUSE
}
