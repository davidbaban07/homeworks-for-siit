public enum BedType {
    SINGLE,
    DOUBLE,
    KING_SIZE,
    QUEEN_SIZE
}
