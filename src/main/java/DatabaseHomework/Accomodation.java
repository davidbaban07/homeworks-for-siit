public class Accomodation {
    private int id;
    private RoomType roomType;
    private BedType bedType;
    private int max_guests;
    private String description;


    public Accomodation(RoomType roomType, BedType bedType, int max_guests, String description) {
        this.roomType = roomType;
        this.bedType = bedType;
        this.max_guests = max_guests;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public BedType getBedType() {
        return bedType;
    }

    public void setBedType(BedType bedType) {
        this.bedType = bedType;
    }

    public int getMax_guests() {
        return max_guests;
    }

    public void setMax_guests(int max_guests) {
        this.max_guests = max_guests;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Accomodation{" +
                "id=" + id +
                ", roomType=" + roomType +
                ", bedType=" + bedType +
                ", max_guests=" + max_guests +
                ", description='" + description + '\'' +
                '}';
    }
}
