public class BookingFailureException extends Exception{
    BookingFailureException(String message)
    {
        super(message);
    }
}
