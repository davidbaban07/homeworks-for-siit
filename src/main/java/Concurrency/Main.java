package Concurrency;

import java.util.Random;

public class Main {
    public static void main(String[] args) throws InterruptedException {


        FestivalGate festivalGate=new FestivalGate();
        FestivalStatisticsThread statistics = new FestivalStatisticsThread(festivalGate);

        statistics.start();
        for(int i = 1; i<=100;i++)
        {
            //generate random ticket types
            TicketType ticketType = TicketType.values()[new Random().nextInt(TicketType.values().length)];
            Thread festivalAttendee=new FestivalAttendeeThread(ticketType,festivalGate);
            //opening gate
            festivalAttendee.start();
            Thread.sleep(500);
            festivalAttendee.join();
        }

    }
}
