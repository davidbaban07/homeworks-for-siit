package Concurrency;

public class FestivalStatisticsThread extends Thread{

    FestivalGate festivalGate;

    FestivalStatisticsThread(FestivalGate festivalGate)
    {
        this.festivalGate=festivalGate;
    }
    @Override
    public void run() {

        do{
            festivalGate.count();
            try{
                //every 5 seconds display how much tickets were sold
                Thread.sleep(5000);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            //this will execute util tickets will be sold out
        }while (festivalGate.tickets!=null);

    }

}
