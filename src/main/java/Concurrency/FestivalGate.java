package Concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FestivalGate {

    List<TicketType> tickets=new ArrayList<>();

    public void addTicket(TicketType ticketType)
    {
        tickets.add(ticketType);
    }


    //counting and displaying ticket types

    public void count()
    {
        System.out.println("Tickets sold : " + tickets.size());
        System.out.println("Full tickets: " + Collections.frequency(tickets, TicketType.FULL));
        System.out.println("Full-VIP tickets: " + Collections.frequency(tickets, TicketType.FULLVIP));
        System.out.println("FreePass tickets: " + Collections.frequency(tickets, TicketType.FREEPASS));
        System.out.println("OneDay tickets: " + Collections.frequency(tickets, TicketType.ONEDAY));
        System.out.println("OneDayVIP tickets: " + Collections.frequency(tickets, TicketType.ONEDAYVIP));
    }
}
