package Concurrency;

public enum TicketType {
    FULL,
    FULLVIP,
    FREEPASS,
    ONEDAY,
    ONEDAYVIP
}
