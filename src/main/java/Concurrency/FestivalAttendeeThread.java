package Concurrency;

public class FestivalAttendeeThread  extends Thread{

    private final TicketType ticketType;

    FestivalAttendeeThread(TicketType ticketType, FestivalGate festivalGategate)
    {
        this.ticketType=ticketType;
        festivalGategate.addTicket(ticketType);
    }
    @Override
    public void run()
    {
        System.out.println("Ticket : " + ticketType);
    }

}
