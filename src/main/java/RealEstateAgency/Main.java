package RealEstateAgency;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        //se creeaza obiectele , in cazul nostru cei doi utilizatori

        User david=new User();
        User adrian=new User();

        //apelarea metodelor setter prin care avem acces la variabilele private(Incapsularea datelor)

        david.setName("David");
        david.setLastName("Baban");

        //apelarea metodelor setter prin care avem acces la variabilele private(Incapsularea datelor)

        adrian.setName("Adrian");
        adrian.setLastName("Muresan");

        //afisarea numelor userilor prin metode getter

        System.out.println("Numele celor doi useri : ");

        System.out.println(david.getFirstName() + " " + david.getLastName());
        System.out.println(adrian.getFirstName()+ " " + adrian.getLastName());

        System.out.println("Inainte de a atribui apartamentele lui David: ");
        System.out.println(david + "\n");

        System.out.println("Inainte de a atribui apartamentele lui Adrian: ");
        System.out.println(adrian+ "\n");

        System.out.println("Apartamente : \n");

        //Se creaza obiectul de tip Apartments + se folosesc metode setter pentru utilizarea atributelor private
        Apartments apartament1 = new Apartments();
        apartament1.setAddress("Strada Somesului nr 17 ");
        apartament1.setRent(450);
        apartament1.setOwner("Baban David ");
        apartament1.setTenant("Pop Ioan ");
        System.out.println( "Adresa: " + apartament1.getAdress() + "Pret chirie: " +  apartament1.getRent() + "Proprietar:  " + apartament1.getOwner()
                + "Chirias:  " +  apartament1.getTenant());

        Apartments apartament2 = new Apartments();
        apartament2.setAddress("Strada Buna Ziua 82 ");
        apartament2.setRent(350);
        apartament2.setOwner("Muresan Adrian ");
        apartament2.setTenant("Marcel Alexandru ");
        System.out.println( "Adresa: " + apartament2.getAdress() + "Pret chirie: " +  apartament2.getRent() + "Proprietar:  " + apartament2.getOwner()
                + "Chirias:  " +  apartament2.getTenant());

        Apartments apartament3 = new Apartments();
        apartament3.setAddress("Strada Aurel Vlaicu 12 ");
        apartament3.setRent(500.5);
        apartament3.setOwner("Muresan Adrian ");
        apartament3.setTenant("Iuliu Dragos ");
        System.out.println( "Adresa: " + apartament3.getAdress() + "Pret chirie: " +  apartament3.getRent() + "Proprietar:  " + apartament3.getOwner()
                + "Chirias:  " +  apartament3.getTenant() + "\n");

        //varianta prin care variabilele din clase erau publice

        /*.firstName="Baban";
        david.lastName="David";
        adrian.firstName="Muresan";
        adrian.lastName="Adrian";


        apartament1.address="Strada Someuslui 17";
        apartament1.owner="Muresan Adrian";
        apartament1.tenant="Pop Ioan";
        apartament1.rent=450;

        Apartments apartament2 = new Apartments();
        apartament2.address="Strada Buna Ziua 82";
        apartament2.owner="Marcel Alexandru";
        apartament2.tenant="Baban David";
        apartament2.rent=350;

        Apartments apartament3=new Apartments();
        apartament3.address="Strada Aurel Vlaicu 12";
        apartament3.owner="Muresan Adrian";
        apartament3.tenant="Iuliu Dragos";
        apartament3.rent = 500.5;*/

        System.out.println("Afisarea dupa atriburiea apartamentelor lui David si Adrian");

        david.addApartment(apartament1);
        adrian.addApartment(apartament2);
        adrian.addApartment(apartament3);

        System.out.println(david);
        System.out.println(adrian);

        //Stergem un apartament din lista lui Adrian

        System.out.println("Adrian vine Apartamentul3: ");
        adrian.removeApartment(apartament3);
        System.out.println(adrian);


    }


}
