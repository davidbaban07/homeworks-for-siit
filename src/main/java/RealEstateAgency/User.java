package RealEstateAgency;

import java.util.ArrayList;
import java.util.List;
import java.lang.String;

public class User {
    //atribute private
    private String firstName;
    private String lastName;
    private List<Apartments> apartment = new ArrayList<>();

    //metoda pentru adaugarea unui apartament
    void addApartment(Apartments apartaments) {
        this.apartment.add(apartaments);
    }
    //metoda pentru stergerea unui apartament
    void removeApartment(Apartments apartments){
        this.apartment.remove(apartments);
    }
    //metode getter
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public List getApartment()
    {
        return apartment;
    }

    //metode setter

    public void setName(String newName) {
        this.firstName = newName;
    }
    public void setLastName(String newLastname) {
        this.lastName = newLastname;
    }
    public void setApartment(List newApartment) {
        this.apartment = newApartment;
    }


    @Override
    public String toString() {
        return "main.java.ro.sci.Main.User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", apartment=" + apartment +
                '}';
    }
}