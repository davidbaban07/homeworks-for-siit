package RealEstateAgency;

public class Apartments {

    //atribute private

    private String address;
    private double rent;
    private String owner;
    private String tenant;



    //metode getter

    public String getAdress()
    {
        return address;
    }
    public double getRent()
    {
        return rent;
    }
    public String getOwner()
    {
        return owner;
    }
    public String getTenant()
    {
        return tenant;
    }
    //metode setter

    public void setAddress(String newAddress)
    {
        this.address=newAddress;
    }

    public void setRent(double newRent)
    {
        this.rent=newRent;
    }

    public void setOwner(String newOwner)
    {
        this.owner=newOwner;
    }
    public void setTenant(String newTenant){

        this.tenant=newTenant;
    }


    @Override
    public String toString() {
        return "Apartments{" +
                "address='" + address + '\'' +
                ", rent=" + rent +
                ", owner='" + owner + '\'' +
                ", tenant='" + tenant + '\'' +
                '}';
    }
}
