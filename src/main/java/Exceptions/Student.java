package Exceptions;

import java.time.LocalDate;
import java.time.Period;
import java.util.Comparator;
import java.util.logging.Logger;

public class Student {

    private String firstName;
    private String lastName;
    protected LocalDate dateOfBirth;
    private String gender;
    protected String id;
    protected Logger logger = Logger.getLogger("Logger");
    protected LocalDate today=LocalDate.now();

    public Student() {
    }
    public Student(String firstName, String lastName, LocalDate dateOfBirth, String gender, String id) {
        setFirstName(firstName);
        setLastName(lastName);
        setDateOfBirth(dateOfBirth);
        setGender(gender);
        setId(id);
    }
    public boolean validateBirthday()
    {
        return (dateOfBirth.isAfter(LocalDate.parse("1900-01-01")) || dateOfBirth.isBefore(LocalDate.parse("2022-01-01")));
    }

    public int calculateAge(LocalDate birthDate, LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        }
        else
        {
            return 0;
        }
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        if (firstName.isEmpty()) {
            logger.info("Invalid firstname");
            throw new IllegalArgumentException("Invalid firstname");
        }
        else
        {
            this.firstName=firstName;
        }
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        if (lastName.isEmpty()) {
            logger.info("Invalid lastname");
            throw new IllegalArgumentException("Invalid lastname");
        }
        else{
            this.lastName=lastName;
        }
    }
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(LocalDate dateOfBirth) {
        if(dateOfBirth.isAfter(LocalDate.parse("1900-01-01")) && (dateOfBirth.isBefore(LocalDate.parse("2018-01-01"))))
        {
            this.dateOfBirth= dateOfBirth;
        }
        else
        {
            logger.info("Invalid birthday");
            throw new IllegalArgumentException("Invalid birthday");
        }
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        if(gender.equalsIgnoreCase("M") || gender.equalsIgnoreCase("F"))
        {
            this.gender=gender;
        }
        else
        {
            logger.info("Invalid gender");
            throw new IllegalArgumentException("Invalid gender");
        }
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        if(id.length() == 13)
        {
            this.id=id;
        }
        else
        {
            logger.info("Invalid ID");
            throw new IllegalArgumentException("Invalid ID");
        }
    }
    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", gender='" + gender + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
