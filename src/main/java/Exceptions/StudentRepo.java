package Exceptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StudentRepo extends Student {

    private List<Student> studentRepo=new ArrayList<>();

    public void addStudent(Student student)
    {
        studentRepo.add(student);
    }

    public void removeStudent(String id) {

            if (id.isEmpty() )
            {
                throw new IllegalArgumentException();
            }
                studentRepo.removeIf(student -> student.getId().equals(id));
                System.out.println("New list after remove : " + studentRepo);
    }
    public List<Student> listStudentsWithAge(int age)
    {

            if(age<0) throw new IllegalArgumentException();

            List<Student> generatedList=new ArrayList<>();
            for (Student student : studentRepo) {
                if (age == calculateAge(student.getDateOfBirth(), today)) {
                    generatedList.add(student);
                }
            }
        return generatedList;
    }

    static class NameComparator implements Comparator<Student>
    {
        @Override
        public int compare(Student o1, Student o2)
        {
            return o1.getLastName().compareTo(o2.getLastName());
        }
    }

    public void sortStudents()
    {
        if(studentRepo.isEmpty())
        {
            logger.info("Empty repO");
            throw new RuntimeException("There are no students");
        }
        else
        {
            studentRepo.sort(new NameComparator());
            System.out.println(studentRepo);
        }
    }

    @Override
    public String toString() {
        return "StudentRepo{" +
                "studentRepo=" + studentRepo +
                '}';
    }
}
