package Exceptions;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        //throws firstname exception
        //Student student1=new Student("" , "Baban" , LocalDate.parse("1999-12-27"),"M","1996227060453");

        //throws lastname exception
        //Student student2=new Student("David" , " " , LocalDate.parse("1999-12-27"),"M","1996227060453");

        //throws birthday exception
        //Student student3=new Student("David" , "Baban" , LocalDate.parse("1111-12-27"),"M","1996227060453");

        //throws gender exception
        //Student student3=new Student("David" , "Baban" , LocalDate.parse("1999-12-27"),"qasd","1996227060453");


        Student student1=new Student("David" , "Baban" , LocalDate.parse("1999-12-27"),"M","1996227060453");
        Student student2=new Student("Radu" , "Zop" ,LocalDate.parse("1999-12-01") ,"m","1991357060443");
        Student student3=new Student("Alex" , "Pop" ,LocalDate.parse("1999-10-27") ,"m","1991357060443");

        StudentRepo studentRepo=new StudentRepo();

        studentRepo.addStudent(student1);
        studentRepo.addStudent(student2);
        studentRepo.addStudent(student3);

        System.out.println(studentRepo);

        System.out.println(studentRepo.listStudentsWithAge(22));

        studentRepo.removeStudent("1991357060443");

        studentRepo.sortStudents();



    }
}
