package ObjectContainers;

import java.util.ArrayList;
import java.util.Collections;

public class Hobby {

    private String hobbyName ;
    private int frequency;
    private ArrayList<Address> addresses=new ArrayList();

    public Hobby(String hobbyName, int frequency, Address... addresses) {
        this.hobbyName = hobbyName;
        this.frequency = frequency;
        Collections.addAll(this.addresses, addresses);
    }

    public String getHobbyName() {
        return hobbyName;
    }

    public int getFrequency() {
        return frequency;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "hobbyName='" + hobbyName + '\'' +
                ", frequency=" + frequency +
                ", addresses=" + addresses +
                '}';
    }
}
