package ObjectContainers;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        ArrayList<Person> personList = new ArrayList<>();
        personList.add(new Student("David Baban", 22, "1991225337894"));
        personList.add(new Unemployed("Adrian Pop", 25, "1995335770532"));
        personList.add(new Hired("Alexandra Blaga", 30, "1991226606778"));
        personList.add(new Student("Alex Muresan", 18, "1992223446789"));
        personList.add(new Unemployed("Vasile Zegrean", 23, "1995003223553"));
        personList.add(new Hired("Bogdan Rares", 24, "1994995664223"));

        Set<Person> ageOrdered = new TreeSet<>(new AgeComparator());
        ageOrdered.addAll(personList);
        System.out.println("Lista  ordonata dupa varsta este : " + ageOrdered);

        Set<Person> nameOrdered = new TreeSet<>(new NameComparator());
        nameOrdered.addAll(personList);
        System.out.println("Lista  ordonata dupa nume este : " + nameOrdered);


        Set<Hobby> hobbies = new HashSet<>();
        Address address1 = new Address("Austria", "Viena", "Stalz", "25");
        Address address2 = new Address("Germany", "Munich", "Aiched", "14");
        Address address3 = new Address("Italy", "Roma", "Giorno", "45");
        Address address4 = new Address("Romania", "Cluj-Napoca", "Buna Ziua", "18");

        Hobby karting = new Hobby("karting", 5, address1, address2);
        Hobby skydiving = new Hobby("skydiving", 2, address4, address3);
        Hobby soccer = new Hobby("soccer", 4, address2, address3, address4);
        Hobby scubadiving = new Hobby("scubadiving", 4, address3, address1);

        hobbies.add(karting);
        hobbies.add(skydiving);
        hobbies.add(soccer);
        hobbies.add(scubadiving);

        ArrayList<Hobby> hobbiesList = new ArrayList(hobbies);
        Person person1 = personList.get(0);

        HashMap<Person, List<Hobby>> personAndHobbies = new HashMap<>();
        personAndHobbies.put(person1, hobbiesList);

        System.out.println("Hobbies for person: " + person1.getName());
        personAndHobbies.get(person1).forEach(x -> {
            System.out.println(x.getHobbyName() + " is practiced in:");
            x.getAddresses().forEach(y -> System.out.println(y.getCountry()));
            System.out.println();
        });


    }
}
