package IntoTheJava;

public class Prime {
    //ne vom folosi de doua functii , una pentru verificare, si cealalta pentru afisare
    public static boolean verificarePrime(int n) {

        if (n <= 1)
            return false;
        //parcurgere , daca numarul se imparte la i , atunci numarul nu este prim
        for (int i = 2; i < n; i++)
            if (n % i == 0) {
                return false;
            }
        return true;
    }

    //afisare numere prime
    public static void afisarePrime(int n) {
        for (int i = 2; i <= n; i++) {
            if (verificarePrime(i))
                System.out.print( i + " ");
        }
    }
}