package IntoTheJava;

public class Smallest {
    //algoritm bubblesort, principiul algoritmului este de a muta numerele mari din sir la sfarsitul sirului
    public int bubbleSort(int [] a , int lungime)

    {
        int auxiliar;
        //parcurgere cu 2 for-uri , pentru a compara doua cate doua variabile
        for(int i=0 ; i < lungime-1; i++)
        {
            for(int j=i+1; j< lungime; j++ )
            {
                //comparare
                if(a[i]>a[j])
                {
                    //interschimbarea variabilelor
                    auxiliar=a[i];
                    a[i]=a[j];
                    a[j]=auxiliar;
                }
            }
        }
        //returnam primul element
        return a[0];
    }

}
