package IntoTheJava;

public class MaxDigit {

    public static int maxim(int numar){

        int largest=0;
        int reminder;
        while(numar!=0)
        {
            //prin %10 obtinem restul impartirii, acesta fiind ultima cifra din numar
            reminder=numar%10;

                if(largest<reminder)
                {
                    //in variabila largest stocam cea mai mare  cifra
                    largest=reminder;
                }
            //prin /10 eliminam cate o cifra din numar , pana cand numarul o sa devina 0
            numar=numar/10;
        }
        return largest;
    }
}
