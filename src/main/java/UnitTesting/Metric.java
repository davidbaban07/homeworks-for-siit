package UnitTesting;

public enum Metric {
    mm(1),
    cm(10),
    dm(100),
    m(1000),
    km(1000000);

    private int multiplier;

    Metric(int multiplier)
    {
        this.multiplier=multiplier;
    }
    public int getMultiplier()
    {
        return multiplier;
    }


}
