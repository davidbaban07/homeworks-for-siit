package UnitTesting;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Calculator {

    private String expression;
    private Metric specifiedMetric;
    private String[] elements;

    public Calculator(String expression, Metric specifiedMetric)
    {
        this.expression=expression;
        this.specifiedMetric=specifiedMetric;
        elements = expression.split(" ");
    }
   private static boolean isNumeric(String str)
   {
       return str !=null && str.matches("[-+]?\\d*\\.?\\d+");
   }

    public double calculate() {

        double result = 0;
        List<Double> values = new ArrayList<>();

        for (int i = 0; i < elements.length; i++) {
            if (isNumeric(elements[i])) {
                int multiplier=1;
                int operator=1;

                for (Metric m : Metric.values()) {
                    if (m.name().equals(elements[i + 1])) {
                        multiplier = m.getMultiplier();
                    }
                }
                if (i >= 1) {
                    if (elements[i - 1].equals("-")) {
                        operator = -1;
                    }
                }
                values.add((Double.parseDouble(elements[i]) * multiplier * operator));
            }
        }
        for (Double value : values) {
            result += value;
        }
        result /= specifiedMetric.getMultiplier();

        return result;
    }

    public String getMetric() {
        return specifiedMetric.name();
    }


    @Override
    public String toString() {
        return "Calculator{" +
                "expression='" + expression + '\'' +
                ", specifiedMetric=" + specifiedMetric +
                ", elements=" + Arrays.toString(elements) +
                '}';
    }
}
