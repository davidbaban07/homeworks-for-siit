package IO;

public class Athlete {

    private final int atheleNumber;
    private final String athleteName;
    private final String countryCode;
    private final String SkiTimeResult;
    private final String firstShootingRange;
    private final String secondShooting;
    private final String thirdShooting;
    private int penalty;
    private int totaltime;

    public Athlete(int atheleNumber, String athleteName, String countryCode, String skiTimeResult, String firstShootingRange, String secondShooting, String thirdShooting) {
        this.atheleNumber = atheleNumber;
        this.athleteName = athleteName;
        this.countryCode = countryCode;
        this.SkiTimeResult = skiTimeResult;
        this.firstShootingRange = firstShootingRange;
        this.secondShooting = secondShooting;
        this.thirdShooting = thirdShooting;
    }

    public int getTotaltime()
    {
        return totaltime;
    }
    public void calculateTime()
    {
        String [] time=SkiTimeResult.split(":");
        int minutes=Integer.parseInt(time[0])*60;
        int seconds=Integer.parseInt(time[1]);

        String shootingResults=firstShootingRange+secondShooting+thirdShooting;

        for(int i=0;i<shootingResults.length();i++)
        {
            if(shootingResults.charAt(i) == 'o')
            {
                penalty+=10;
            }
        }
        totaltime=minutes+seconds+penalty;
    }

    @Override
    public String toString() {
        return "Athlet{" +
                "atheleNumber=" + atheleNumber +
                ", athleteName='" + athleteName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", SkiTimeResult='" + SkiTimeResult + '\'' +
                ", FirstShootingRange='" + firstShootingRange + '\'' +
                ", SecondShooting='" + secondShooting + '\'' +
                ", ThirdShooting='" + thirdShooting + '\'' +
                '}';
    }

}
