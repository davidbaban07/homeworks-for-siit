package IO;

import java.io.*;
import java.util.TreeSet;

public class Standings {
    private final TreeSet<Athlete> athletes = new TreeSet<>(new AthleteComparator());
    public void readAthletes()
    {
        String path="src/main/resources/IO_Files/players.csv";
        String line="";
        String[] attributes;

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            while((line=br.readLine())!=null)
            {
                attributes=line.split(",");
                System.out.println(line);
                athletes.add(new Athlete(Integer.parseInt(attributes[0]), attributes[1], attributes[2], attributes[3], attributes[4], attributes[5], attributes[6]));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TreeSet<Athlete> getAthletes() {
        return athletes;
    }

    public void printResults() {
        System.out.println("Results: ");
        int i = 1;
        for (Athlete athlete : athletes) {
            System.out.println("Ranking " + i + " " + athlete);
            i++;
        }
    }

    @Override
    public String toString() {
        return "Standings{" +
                "athletes=" + athletes +
                '}';
    }
}
