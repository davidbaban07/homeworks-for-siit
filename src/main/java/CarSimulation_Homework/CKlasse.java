package CarSimulation_Homework;

public class CKlasse extends Mercedes{

    public final int tankSize=90;
    public final String fuelType="Diesel";
    public final double fuelConsumption=13;

    CKlasse(int available_fuel, String chassisNumber) {
        this.availableFuel=available_fuel;
        this.chassisNumber = chassisNumber;

    }
    public double getFuelConsumption() {
        return this.fuelConsumption;
    }

    @Override
    public void changeTireSize(int tireSize) {
        this.tireSize=tireSize;
    }
    public double getAverageFuelConsumption() {
        return fuelConsumption;
    }
}
