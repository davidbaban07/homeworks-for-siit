package CarSimulation_Homework;

public abstract class Car implements Vehicle{

    private static final int fuelTankSize=80;
    private static final String fuelType="Diesel";
    private static final double consumptionPer100Km=8.5;
    private static final int gears = 6;
    protected double availableFuel;
    protected int tireSize;
    protected String chassisNumber;
    protected double fuelConsumed;
    private boolean vehicleSituation;
    private int currentGear=0;

    public void shiftGear(int gear) {
        if ((gear > 6) ||  (gear < 0)) {
            System.err.println("Invalid Gear");
        } else
            currentGear = gear;
    }
    @Override
    public void start() {
        System.out.println("Engine started");
        System.out.println("Available fuel: " + availableFuel);
        fuelConsumed=0;
        vehicleSituation=true;
    }
    @Override
    public void stop() {
        System.out.println("Engine stopped");
        vehicleSituation=false;
    }

    @Override
    public void drive(double distance) {
        if(vehicleSituation)
        {
            double consumption = ((double) (gears - currentGear) / 10) * getFuelConsumption();
            fuelConsumed=(distance*(getFuelConsumption()+ consumption) / 100);
            availableFuel-=fuelConsumed;
            System.out.println("Car driven " + distance + " kilometers and used " + fuelConsumed + " liters");
        } else System.out.println("Car is stopped");
    }



    public double getAvailableFuel() {
        return availableFuel;
    }

    public void setAvailableFuel(double availableFuel) {
        this.availableFuel = availableFuel;
    }

    public double getAverageFuelConsumption() {
        return consumptionPer100Km;
    }
    }

