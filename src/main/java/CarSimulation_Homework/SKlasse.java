package CarSimulation_Homework;

public class SKlasse extends Mercedes{

    public final int tankSize=100;
    public final String fuelType="Gas";
    public final double fuelConsumption=15;

    SKlasse(int available_fuel, String chassisNumber) {
        this.availableFuel=available_fuel;
        this.chassisNumber = chassisNumber;

    }
    public double getFuelConsumption() {
        return this.fuelConsumption;
    }

    @Override
    public void changeTireSize(int tireSize) {
        this.tireSize=tireSize;
    }
    public double getAverageFuelConsumption() {
        return fuelConsumption;
    }
}
