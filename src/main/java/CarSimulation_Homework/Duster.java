package CarSimulation_Homework;

public class Duster extends Dacia{

    public final int tankSize=90;
    public final String fuelType="Diesel";
    public final double fuelConsumption=10;

    Duster(int available_fuel, String chassisNumber) {
        this.availableFuel=available_fuel;
        this.chassisNumber = chassisNumber;

    }
    public double getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public void changeTireSize(int tireSize) {
        this.tireSize=tireSize;
    }
    public double getAverageFuelConsumption() {
        return fuelConsumption;
    }
}
