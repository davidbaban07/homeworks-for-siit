package CarSimulation_Homework;

public interface Vehicle {

    void start();
    void stop();
    void drive(double distance);
    double getFuelConsumption();
    void changeTireSize(int size);
    double getAvailableFuel();
    double getAverageFuelConsumption();
}
