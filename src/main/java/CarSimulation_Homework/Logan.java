package CarSimulation_Homework;

public class Logan extends Dacia{
        public final int tankSize=60;
        public final String fuelType="Diesel";
        public final double fuelConsumption=5.5;

    Logan(int available_fuel, String chassisNumber) {
        this.availableFuel=available_fuel;
        this.chassisNumber = chassisNumber;

    }
    public double getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public void changeTireSize(int tireSize) {
        this.tireSize=tireSize;
    }
    public double getAverageFuelConsumption() {
        return fuelConsumption;
    }
}
