package CarSimulation_Homework;

public class Main {
    public static void main(String[] args) {

        Car logan=new Logan(45, "2fg67g346");
        logan.start();
        logan.shiftGear(1);
        logan.drive(0.01);
        logan.shiftGear(2);
        logan.drive(0.02);
        logan.shiftGear(3);
        logan.drive(2);
        logan.shiftGear(4);
        logan.drive(5);
        logan.shiftGear(5);
        logan.drive(20);
        logan.shiftGear(4);
        logan.drive(0.5);
        logan.shiftGear(3);
        logan.drive(0.1);
        logan.stop();
        logan.changeTireSize(20);

        float availableFuel=(float)logan.getAvailableFuel();
        System.out.println("Available fuel on Logan: " + availableFuel);
        float consumptionPer100Km=(float)logan.getAverageFuelConsumption();
        System.out.println("Average car fuel consumption on Logan: " + consumptionPer100Km);

        logan.start();

        Vehicle duster = new Duster(70 , "e345ggdfsa3");
        duster.start();
        duster.drive(50);
        duster.stop();

        Car car = (Car) duster;

        float availableFuel_duster=(float)duster.getAvailableFuel();
        System.out.println("Available fuel on Duster: " + availableFuel_duster);
        float consumptionPer100Km_duster=(float)duster.getAverageFuelConsumption();
        System.out.println("Average car fuel consumption on Duster: " + consumptionPer100Km_duster);

        duster.start();

        Car sklasse = new SKlasse(90 , "ezz45ghfsa3");
        sklasse.start();
        sklasse.drive(90);
        sklasse.stop();

        float availableFuel_sklasse=(float)sklasse.getAvailableFuel();
        System.out.println("Available fuel on Sklasse: " + availableFuel_sklasse);
        float consumptionPer100Km_sklasse=(float)sklasse.getAverageFuelConsumption();
        System.out.println("Average car fuel consumption on Sklasse: " + consumptionPer100Km_sklasse);

        Car cklasse = new CKlasse(80 , "ea55ghmgfsa3");
        cklasse.start();
        cklasse.drive(50);
        cklasse.stop();

        float availableFuel_cklasse=(float)cklasse.getAvailableFuel();
        System.out.println("Available fuel on Cklasse: " + availableFuel_cklasse);
        float consumptionPer100Km_cklasse=(float)cklasse.getAverageFuelConsumption();
        System.out.println("Average car fuel consumption on Cklasse: " + consumptionPer100Km_cklasse);
    }
}
