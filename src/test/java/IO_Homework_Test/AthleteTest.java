package IO_Homework_Test;
import IO.Athlete;
import IO.Standings;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AthleteTest {

    Standings standings=new Standings();

    @Test
    public void calculateTotalTime()
    {
        Athlete athlete=new Athlete(07,"David Baban","RO","29:40","xxxoo","xxooo","xxoxo");
        athlete.calculateTime();
        assertEquals(1850,athlete.getTotaltime());
    }
    @Before
    public void setUp(){
        standings.readAthletes();
    }
    @Test
    public void readAthletesTest() {

        assertEquals(1, standings.getAthletes().size());

    }


}
