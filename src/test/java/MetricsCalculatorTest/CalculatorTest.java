package MetricsCalculatorTest;

import  UnitTesting.Calculator;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static UnitTesting.Metric.*;


public class CalculatorTest {
    private static final double DELTA = 1e-15;
    private Calculator testCalculator;

    @Test
    public void calculateTestWithSpecificValues() {
        testCalculator = new Calculator("12 cm - 1 m + 17 mm" , dm);
        assertEquals(-8.63, testCalculator.calculate(), DELTA);
    }
    @Test
    public void calculateTestWithOnlyPositiveValues(){
        testCalculator=new Calculator("12 km + 10 dm + 3 m",m);
        assertEquals(12004.0,testCalculator.calculate(),DELTA);
    }
    @Test
    public void calculateTestWithOnlyNegativeValues(){
        testCalculator=new Calculator("-10 km - 13 m - 2 dm",m);
        assertEquals(-10013.2,testCalculator.calculate(),DELTA);
    }
    @Test
    public void getMetricAsStringReturnsCorrectValue(){
        testCalculator=new Calculator("-10 km - 13 m - 2 dm",m);
        String test=testCalculator.getMetric();
        assertEquals("m",test);
    }








}
