package MetricsCalculatorTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CalculatorTest.class, ParameterisedTest.class})

public class TestSuite {
}