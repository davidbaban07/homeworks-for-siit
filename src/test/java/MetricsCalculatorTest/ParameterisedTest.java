package MetricsCalculatorTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import  UnitTesting.Calculator;
import  UnitTesting.Metric;
import static org.junit.Assert.assertEquals;
import static UnitTesting.Metric.*;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class ParameterisedTest {

    private static Calculator calculator;

    private String expression;
    private Metric metric;
    private double expected;

    @Parameterized.Parameters
    public static List<Object> data() {
        return Arrays.asList(new Object[][]{
                {"1 dm - 5 cm", mm, 50},
                {"1000 m + 100000 cm", km, 2},
                {"2000 cm + 100000 cm", m, 1020}
        });
    }

    public ParameterisedTest(String expression, Metric metric, double expected) {
        this.expression = expression;
        this.metric = metric;
        this.expected = expected;
        calculator = new Calculator(expression, metric);
    }
    @Test
    public void testCalculator()  {
        assertEquals(calculator.calculate(), expected, 0.001);
    }

}
