package Java8Tests;
import Java8Streams.PersonsSorting;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class PersonsTest {
    private PersonsSorting personsSorting;
    @Before
    public void setUp() throws Exception {
        personsSorting = new PersonsSorting();
        personsSorting.readPersons("src/main/resources/Java8Files/in.txt");
    }
    @After
    public void tearDown() throws Exception {
    }
    @Test
    public void orderdByMonthStreamMonthCheck() throws IOException {
        personsSorting.orderdByMonthStream(3);
    }
    @Test
    public void orderdByMonthStreamVerify() throws FileNotFoundException {
        File file = new File("src/main/resources/Java8Files/out.txt");
        if (!file.exists()) throw new AssertionError();
    }
}
