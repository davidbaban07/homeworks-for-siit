package Exceptions_Homework_Tests;


import Exceptions.Student;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import java.time.LocalDate;
import java.util.Iterator;



public class StudentTest {

    Student student = new Student("David" , "Baban" , LocalDate.parse("1999-12-27"),"M","1996227060453");

    @Test
    public void validateFirstnameTest()
    {
        assertThrows(IllegalArgumentException.class, () -> new Student("", "Baban" , LocalDate.parse("1999-12-27"),"M","1996227060453"));
    }
    @Test
    public void validateLastnameTest()
    {
        assertThrows(IllegalArgumentException.class, () -> new Student("David", "" , LocalDate.parse("1999-12-27"),"M","1996227060453"));
    }
    @Test
    public void validatebirthDayTest()
    {
        assertTrue(student.validateBirthday());
    }
    @Test
    public void validateIdTest()
    {
        assertEquals(13, student.getId().length());
    }
    @Test
    public void calculateAgeTest()
    {
        assertEquals(22,student.calculateAge(LocalDate.parse("1999-12-27"),LocalDate.parse("2022-01-01")));
    }
    @Test
    public void validateGenderTest()
    {
        assertThrows(IllegalArgumentException.class, () -> new Student("David", "Baban" , LocalDate.parse("1999-12-27"),"r","1996227060453"));
    }



}
