package Exceptions_Homework_Tests;

import Exceptions.Student;
import Exceptions.StudentRepo;
import org.junit.Test;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class StudentRepoTest {
    StudentRepo studentRepo=new StudentRepo();

    Student student1=new Student("David" , "Baban" , LocalDate.parse("1999-12-27"),"M","1996227060453");
    Student student2=new Student("Radu" , "Zop" ,LocalDate.parse("1999-12-27") ,"m","1991357060443");
    Student student3=new Student("Alex" , "Pop" ,LocalDate.parse("1999-12-27") ,"m","1991357060443");
    @Test
    public void addStudentsTest()
    {
        studentRepo.addStudent(student1);
        studentRepo.addStudent(student2);
        studentRepo.addStudent(student3);

        assertEquals(3,studentRepo.listStudentsWithAge(22).size());
    }
    @Test
    public void deleteStudentWrongIDTest() {

        assertThrows(IllegalArgumentException.class , ()-> studentRepo.removeStudent(""));
    }
    @Test
    public void getStudentsWithNegativeAgeTest()
    {
        assertThrows(IllegalArgumentException.class , () -> studentRepo.listStudentsWithAge(-1));
    }


}
